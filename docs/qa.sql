-- ======================================================================================
-- Create SQL Login template for Azure SQL Database and Azure SQL Data Warehouse Database
-- ======================================================================================

CREATE LOGIN encuesta_user
	WITH PASSWORD = '1Tennis1*' 
GO
CREATE USER encuesta_user FROM LOGIN encuesta_user;
ALTER ROLE dbmanager ADD MEMBER encuesta_user; 