﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebEncuesta.Migrator;

namespace WebEncuesta.Migrator.Migrations
{
    [DbContext(typeof(ProxyWebContext))]
    [Migration("20181005053810_SimplificaUsuario")]
    partial class SimplificaUsuario
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.2-rtm-30932")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebEncuesta.Domain.Model.Area", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Nombre");

                    b.HasKey("Id");

                    b.ToTable("Areas");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Auditorias.RegistroAuditoria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EncuestaId");

                    b.Property<DateTime>("Fecha");

                    b.Property<string>("Mensaje");

                    b.Property<string>("QueCambio");

                    b.Property<int>("UsuarioId");

                    b.Property<string>("ValorNuevo");

                    b.Property<string>("ValorOriginal");

                    b.HasKey("Id");

                    b.HasIndex("EncuestaId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("RegistroAuditoria");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Cliente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Rut");

                    b.HasKey("Id");

                    b.ToTable("Clientes");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Encuesta", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descripcion");

                    b.Property<bool>("Deshabilitada");

                    b.Property<int?>("SeccionId");

                    b.Property<string>("Titulo");

                    b.HasKey("Id");

                    b.HasIndex("SeccionId");

                    b.ToTable("Encuestas");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Pregunta", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Deshabilitado");

                    b.Property<int>("EncuestaId");

                    b.Property<bool>("EsPreguntaSnex");

                    b.Property<string>("Glosa");

                    b.Property<int>("Orden");

                    b.Property<int>("TipoPregunta");

                    b.HasKey("Id");

                    b.HasIndex("EncuestaId");

                    b.ToTable("Preguntas");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Respuesta", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("EncuestaId");

                    b.Property<DateTime>("Fecha");

                    b.Property<int?>("RutCliente");

                    b.Property<int?>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("EncuestaId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Respuestas");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.RespuestaPregunta", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("FuePreguntaSnex");

                    b.Property<int>("PreguntaId");

                    b.Property<int>("RespuestaId");

                    b.Property<int?>("Valor");

                    b.HasKey("Id");

                    b.HasIndex("PreguntaId");

                    b.HasIndex("RespuestaId");

                    b.ToTable("RespuestasPreguntas");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Seccion", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AreaId");

                    b.Property<string>("Nombre")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("AreaId");

                    b.ToTable("Secciones");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Usuario", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Contrasena");

                    b.Property<string>("Correo");

                    b.Property<bool>("Deshabilitado");

                    b.Property<bool>("EsAdministrador");

                    b.Property<string>("Nombre");

                    b.Property<int?>("SeccionId");

                    b.HasKey("Id");

                    b.HasIndex("Correo")
                        .IsUnique()
                        .HasFilter("[Correo] IS NOT NULL");

                    b.HasIndex("SeccionId");

                    b.ToTable("Usuarios");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Auditorias.RegistroAuditoria", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Encuesta", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Seccion", "Seccion")
                        .WithMany()
                        .HasForeignKey("SeccionId");

                    b.OwnsOne("WebEncuesta.Domain.Model.RangoFecha", "Vigencia", b1 =>
                        {
                            b1.Property<int>("EncuestaId")
                                .ValueGeneratedOnAdd()
                                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                            b1.Property<DateTime?>("Desde");

                            b1.Property<DateTime?>("Hasta");

                            b1.ToTable("Encuestas");

                            b1.HasOne("WebEncuesta.Domain.Model.Encuestas.Encuesta")
                                .WithOne("Vigencia")
                                .HasForeignKey("WebEncuesta.Domain.Model.RangoFecha", "EncuestaId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Pregunta", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Encuestas.Encuesta", "Encuesta")
                        .WithMany("Preguntas")
                        .HasForeignKey("EncuestaId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.Respuesta", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Encuestas.Encuesta", "Encuesta")
                        .WithMany()
                        .HasForeignKey("EncuestaId");

                    b.HasOne("WebEncuesta.Domain.Model.Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId");
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Encuestas.RespuestaPregunta", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Encuestas.Pregunta", "Pregunta")
                        .WithMany()
                        .HasForeignKey("PreguntaId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("WebEncuesta.Domain.Model.Encuestas.Respuesta", "Respuesta")
                        .WithMany("Respuestas")
                        .HasForeignKey("RespuestaId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Seccion", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Area", "Area")
                        .WithMany()
                        .HasForeignKey("AreaId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("WebEncuesta.Domain.Model.Usuario", b =>
                {
                    b.HasOne("WebEncuesta.Domain.Model.Seccion", "Seccion")
                        .WithMany()
                        .HasForeignKey("SeccionId");
                });
#pragma warning restore 612, 618
        }
    }
}
