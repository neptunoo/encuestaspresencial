﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebEncuesta.Migrator.Migrations
{
    public partial class MultiCompania : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EsCompaniaAlt",
                table: "Encuestas",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EsCompaniaAlt",
                table: "Encuestas");
        }
    }
}
