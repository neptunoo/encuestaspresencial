﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebEncuesta.Migrator.Migrations
{
    public partial class SimplificaUsuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Respuestas_Clientes_ClienteId",
                table: "Respuestas");

            migrationBuilder.DropIndex(
                name: "IX_Usuarios_Rut",
                table: "Usuarios");

            migrationBuilder.DropIndex(
                name: "IX_Respuestas_ClienteId",
                table: "Respuestas");

            migrationBuilder.DropColumn(
                name: "IdentificadorExterno",
                table: "Usuarios");

            migrationBuilder.DropColumn(
                name: "Rut",
                table: "Usuarios");

            migrationBuilder.DropColumn(
                name: "FechaTermino",
                table: "Respuestas");

            migrationBuilder.RenameColumn(
                name: "ClienteId",
                table: "Respuestas",
                newName: "RutCliente");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RutCliente",
                table: "Respuestas",
                newName: "ClienteId");

            migrationBuilder.AddColumn<string>(
                name: "IdentificadorExterno",
                table: "Usuarios",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Rut",
                table: "Usuarios",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaTermino",
                table: "Respuestas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_Rut",
                table: "Usuarios",
                column: "Rut",
                unique: true,
                filter: "[Rut] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Respuestas_ClienteId",
                table: "Respuestas",
                column: "ClienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Respuestas_Clientes_ClienteId",
                table: "Respuestas",
                column: "ClienteId",
                principalTable: "Clientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
