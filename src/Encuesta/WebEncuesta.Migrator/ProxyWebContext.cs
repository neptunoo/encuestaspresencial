﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using WebEncuesta.Domain.Data;

namespace WebEncuesta.Migrator
{
    public class ProxyWebContext : WebEncuestaDbContext
    {
        public ProxyWebContext(DbContextOptions<WebEncuestaDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Database=DBEncuesta;Server=localhost;Trusted_Connection=True;");
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ProxyWebContext>
    {
        public ProxyWebContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<WebEncuestaDbContext>();


            builder.UseSqlServer("Database=DBEncuesta;Server=localhost;Trusted_Connection=True");

            return new ProxyWebContext(builder.Options);
        }
    }
}
