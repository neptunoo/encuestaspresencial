﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace WebEncuesta.Tests.Pages.Areas
{
    public class IndexOnGet : IClassFixture<PagesApplicationFactory<Startup>>
    {
        public HttpClient Client { get; }

        public IndexOnGet(PagesApplicationFactory<Startup> factory)
        {
            Client = factory.CreateClient();
        }

        [Fact]
        public async Task ReturnsHomePageWithProductListing()
        {
            var response = await Client.GetAsync("/Areas/Index");
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            Assert.Contains(".NET Bot Black Sweatshirt", stringResponse);
        }
    }
}
