﻿
using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.Tests
{
    public class PagesApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                Environment.SetEnvironmentVariable("bypassMigration", "1");


                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();


                services.AddDbContext<WebEncuestaDbContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                services.AddMvc(opts => { opts.Filters.Add(new AllowAnonymousFilter()); });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<WebEncuestaDbContext>();

                    db.Database.EnsureCreated();
                    db.Areas.Add(new Area { Nombre = "area1" });
                    db.SaveChanges();

                }
            });


        }
    }
}
