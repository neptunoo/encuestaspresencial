﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using WebEncuesta.Domain.Data;
using Xunit;

namespace WebEncuesta.Unitarios.Tests.Pages.Areas
{
    public class IndexPageTests
    {
        [Fact]
        public async Task OnGetAsync_PopulatesThePageModel_WithAListOfMessages()
        {
            // Arrange
            var optionsBuilder = new DbContextOptionsBuilder<WebEncuestaDbContext>().UseInMemoryDatabase("InMemoryDb");
            //#region snippet1
            //var mockAppDbContext = new Mock<WebEncuestaDbContext>(optionsBuilder.Options);
            //var expectedMessages = AppDbContext.GetSeedingMessages();
            //mockAppDbContext.Setup(
            //    db => db.GetMessagesAsync()).Returns(Task.FromResult(expectedMessages));
            //var pageModel = new IndexModel(mockAppDbContext.Object);
            //#endregion

            //#region snippet2
            //// Act
            //await pageModel.OnGetAsync();
            //#endregion

            //#region snippet3
            //// Assert
            //var actualMessages = Assert.IsAssignableFrom<List<Message>>(pageModel.Messages);
            //Assert.Equal(
            //    expectedMessages.OrderBy(m => m.Id).Select(m => m.Text),
            //    actualMessages.OrderBy(m => m.Id).Select(m => m.Text));
            //#endregion
        }
    }
}
