﻿using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data.Migrator;
using System.Reflection;
using System.Linq;
using WebEncuesta.Domain.Model;
using WebEncuesta.Domain.Model.Auditorias;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.Domain.Model.Querys;

namespace WebEncuesta.Domain.Data
{
    public class WebEncuestaDbContext : DbContext
    {
        public WebEncuestaDbContext(DbContextOptions<WebEncuestaDbContext> options) : base(options)
        {
        }

        #region General
        public DbSet<Area> Areas { get; set; }
        public DbSet<Seccion> Secciones { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Encuesta> Encuestas { get; set; }
        public DbSet<Pregunta> Preguntas { get; set; }
        public DbSet<Respuesta> Respuestas { get; set; }
        public DbSet<RespuestaPregunta> RespuestasPreguntas { get; set; }
        public DbQuery<ItemSnexPorUsuario> ItemSnexPorUsuarios { get; set; }

        #endregion


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var fk in modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetForeignKeys()).Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade))
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<Usuario>().HasIndex(b => b.Correo).IsUnique();
            modelBuilder.Entity<RegistroAuditoria>().HasIndex(b => b.EncuestaId);
            modelBuilder.Entity<Encuesta>().HasIndex(b => b.SeccionId);
            modelBuilder.Entity<Respuesta>().HasIndex(b => b.UsuarioId);
            modelBuilder.Entity<Respuesta>().HasIndex(b => b.EncuestaId);
        }



        private static readonly DbMigrator Migrator = new DbMigrator(typeof(DbMigrator).GetTypeInfo().Assembly, "dbo", "WebEncuesta.Domain.Data.Migrations");
        public void RunMigration()
        {
            Migrator.Migrate(Database.GetDbConnection());
        }
    }
}