using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace WebEncuesta.Domain.Data.Migrator
{
    public class DbMigrator
    {
        private readonly Regex _versionNumberRegex = new Regex(@"(?'version'\d+).*\.sql");

        public DbMigrator(Assembly assembly, string schemaName, string manifestPath)
        {
            SchemaName = schemaName;
            Migrations = assembly
                .GetManifestResourceNames()
                .Where(x => x.IndexOf(manifestPath, StringComparison.OrdinalIgnoreCase) > -1
                         && x.EndsWith(".sql", StringComparison.OrdinalIgnoreCase))
                .Select(x => new ResourceMigration(assembly, x, GetFileVersion(x)) as Migration)
                .OrderBy(x => x.Version)
                .ToList();

            var codeMigrations = assembly
                            .GetTypes()
                            .Where(x => typeof(Migration).IsAssignableFrom(x) && x.Namespace.ToLower().Contains(manifestPath.ToLower()))
                            .Select(Activator.CreateInstance)
                            .Cast<Migration>();

            foreach (var migration in codeMigrations)
            {
                Migrations.Add(migration);
            }

            Migrations = Migrations
                .OrderBy(x => x.Version)
                .ToList();

            VersionTable = new CachedDatabaseVersionTable(new DatabaseVersionTable(SchemaName));
            LatestSchemaVersion = Migrations.Any() ? Migrations.Max(x => x.Version) : 0;
        }

        public CachedDatabaseVersionTable VersionTable { get; }
        public string SchemaName { get; }
        private long LatestSchemaVersion { get; }
        private IList<Migration> Migrations { get; }

        public void Migrate(IDbConnection connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            var database = new MigratorDatabase(connection);
            var dbVersion = GetDbVersion(database);

            Migrate(dbVersion, database);
        }

        private void Migrate(long dbVersion, MigratorDatabase database)
        {
            if (dbVersion >= LatestSchemaVersion)
            {
                return;
            }

            using (var transaction = database.BeginTransaction())
            {
                dbVersion = VersionTable.GetCurrentVersionWithLock(database);

                if (dbVersion >= LatestSchemaVersion)
                {
                    return;
                }

                var version = dbVersion;
                var migrations = Migrations.Where(x => x.Version > version);

                foreach (var migration in migrations)
                {
                    if (migration.Version <= dbVersion)
                    {
                        continue;
                    }

                    migration.Execute(database);

                    dbVersion = VersionTable.GetCurrentVersionWithLock(database);

                    if (migration.Version > dbVersion)
                    {
                        VersionTable.SetVersion(database, migration.Version);
                    }
                }
                transaction.Commit();
            }
        }

        private long GetDbVersion(MigratorDatabase database)
        {
            VersionTable.CreateIfNotExisting(database);
            return VersionTable.GetCurrentVersion(database);
        }

        private long GetFileVersion(string name)
        {
            return long.Parse(_versionNumberRegex.Match(name).Groups["version"].Value);
        }
    }
}
