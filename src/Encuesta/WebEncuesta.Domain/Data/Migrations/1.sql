﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Areas] (
    [Id] int NOT NULL IDENTITY,
    [Nombre] nvarchar(max) NULL,
    CONSTRAINT [PK_Areas] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Clientes] (
    [Id] int NOT NULL IDENTITY,
    [Rut] int NOT NULL,
    CONSTRAINT [PK_Clientes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Secciones] (
    [Id] int NOT NULL IDENTITY,
    [Nombre] nvarchar(max) NOT NULL,
    [AreaId] int NOT NULL,
    CONSTRAINT [PK_Secciones] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Secciones_Areas_AreaId] FOREIGN KEY ([AreaId]) REFERENCES [Areas] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Encuestas] (
    [Id] int NOT NULL IDENTITY,
    [Titulo] nvarchar(max) NULL,
    [Descripcion] nvarchar(max) NULL,
    [Vigencia_Desde] datetime2 NULL,
    [Vigencia_Hasta] datetime2 NULL,
    [SeccionId] int NULL,
    [Deshabilitada] bit NOT NULL,
    CONSTRAINT [PK_Encuestas] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Encuestas_Secciones_SeccionId] FOREIGN KEY ([SeccionId]) REFERENCES [Secciones] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Usuarios] (
    [Id] int NOT NULL IDENTITY,
    [Nombre] nvarchar(max) NULL,
    [EsAdministrador] bit NOT NULL,
    [IdentificadorExterno] nvarchar(max) NULL,
    [Contrasena] nvarchar(max) NULL,
    [Correo] nvarchar(450) NULL,
    [Rut] int NULL,
    [SeccionId] int NULL,
    [Deshabilitado] bit NOT NULL,
    CONSTRAINT [PK_Usuarios] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Usuarios_Secciones_SeccionId] FOREIGN KEY ([SeccionId]) REFERENCES [Secciones] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Preguntas] (
    [Id] int NOT NULL IDENTITY,
    [Glosa] nvarchar(max) NULL,
    [Orden] int NOT NULL,
    [EncuestaId] int NOT NULL,
    [TipoPregunta] int NOT NULL,
    [EsPreguntaSnex] bit NOT NULL,
    [Deshabilitado] bit NOT NULL,
    CONSTRAINT [PK_Preguntas] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Preguntas_Encuestas_EncuestaId] FOREIGN KEY ([EncuestaId]) REFERENCES [Encuestas] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [RegistroAuditoria] (
    [Id] int NOT NULL IDENTITY,
    [UsuarioId] int NOT NULL,
    [Fecha] datetime2 NOT NULL,
    [EncuestaId] int NOT NULL,
    [ValorOriginal] nvarchar(max) NULL,
    [ValorNuevo] nvarchar(max) NULL,
    [Mensaje] nvarchar(max) NULL,
    [QueCambio] nvarchar(max) NULL,
    CONSTRAINT [PK_RegistroAuditoria] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RegistroAuditoria_Usuarios_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [Usuarios] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Respuestas] (
    [Id] int NOT NULL IDENTITY,
    [Fecha] datetime2 NOT NULL,
    [UsuarioId] int NULL,
    [ClienteId] int NULL,
    [EncuestaId] int NULL,
    [FechaTermino] datetime2 NULL,
    CONSTRAINT [PK_Respuestas] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Respuestas_Clientes_ClienteId] FOREIGN KEY ([ClienteId]) REFERENCES [Clientes] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Respuestas_Encuestas_EncuestaId] FOREIGN KEY ([EncuestaId]) REFERENCES [Encuestas] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Respuestas_Usuarios_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [Usuarios] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [RespuestasPreguntas] (
    [Id] int NOT NULL IDENTITY,
    [RespuestaId] int NOT NULL,
    [PreguntaId] int NOT NULL,
    [Valor] int NULL,
    [FuePreguntaSnex] bit NOT NULL,
    CONSTRAINT [PK_RespuestasPreguntas] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RespuestasPreguntas_Preguntas_PreguntaId] FOREIGN KEY ([PreguntaId]) REFERENCES [Preguntas] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_RespuestasPreguntas_Respuestas_RespuestaId] FOREIGN KEY ([RespuestaId]) REFERENCES [Respuestas] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Encuestas_SeccionId] ON [Encuestas] ([SeccionId]);

GO

CREATE INDEX [IX_Preguntas_EncuestaId] ON [Preguntas] ([EncuestaId]);

GO

CREATE INDEX [IX_RegistroAuditoria_EncuestaId] ON [RegistroAuditoria] ([EncuestaId]);

GO

CREATE INDEX [IX_RegistroAuditoria_UsuarioId] ON [RegistroAuditoria] ([UsuarioId]);

GO

CREATE INDEX [IX_Respuestas_ClienteId] ON [Respuestas] ([ClienteId]);

GO

CREATE INDEX [IX_Respuestas_EncuestaId] ON [Respuestas] ([EncuestaId]);

GO

CREATE INDEX [IX_Respuestas_UsuarioId] ON [Respuestas] ([UsuarioId]);

GO

CREATE INDEX [IX_RespuestasPreguntas_PreguntaId] ON [RespuestasPreguntas] ([PreguntaId]);

GO

CREATE INDEX [IX_RespuestasPreguntas_RespuestaId] ON [RespuestasPreguntas] ([RespuestaId]);

GO

CREATE INDEX [IX_Secciones_AreaId] ON [Secciones] ([AreaId]);

GO

CREATE UNIQUE INDEX [IX_Usuarios_Correo] ON [Usuarios] ([Correo]) WHERE [Correo] IS NOT NULL;

GO

CREATE UNIQUE INDEX [IX_Usuarios_Rut] ON [Usuarios] ([Rut]) WHERE [Rut] IS NOT NULL;

GO

CREATE INDEX [IX_Usuarios_SeccionId] ON [Usuarios] ([SeccionId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180906050523_Inicial', N'2.1.2-rtm-30932');

GO

