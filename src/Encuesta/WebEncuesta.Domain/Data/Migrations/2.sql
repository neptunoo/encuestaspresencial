﻿SET IDENTITY_INSERT [dbo].[Areas] ON 
GO
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (1, N'Pérdidas totales')
GO
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (2, N'Siniestros')
GO
SET IDENTITY_INSERT [dbo].[Areas] OFF
GO
SET IDENTITY_INSERT [dbo].[Secciones] ON 
GO
INSERT [dbo].[Secciones] ([Id], [Nombre], [AreaId]) VALUES (1, N'Atención presencial', 1)
GO
INSERT [dbo].[Secciones] ([Id], [Nombre], [AreaId]) VALUES (2, N'Nivel Atención 1', 2)
GO
SET IDENTITY_INSERT [dbo].[Secciones] OFF
GO
SET IDENTITY_INSERT [dbo].[Encuestas] ON 
GO
INSERT [dbo].[Encuestas] ([Id], [Titulo], [Descripcion], [Vigencia_Desde], [Vigencia_Hasta], [SeccionId],[Deshabilitada]) VALUES (1, N'Encuesta de satisfacción de pérdidas totales', N'Pensando en el servicio entregado por su ejecutiva, evalue los siguientes aspectos.', CAST(N'2018-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2), 1,0)
GO
INSERT [dbo].[Encuestas] ([Id], [Titulo], [Descripcion], [Vigencia_Desde], [Vigencia_Hasta], [SeccionId],[Deshabilitada]) VALUES (2, N'Encuesta de atención en caso de siniestros', N'Pensando en el servicio entregado por su ejecutiva, evalue los siguientes aspectos.', CAST(N'2018-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2), 2,0)
GO
SET IDENTITY_INSERT [dbo].[Encuestas] OFF
GO
SET IDENTITY_INSERT [dbo].[Preguntas] ON 
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (1, N'Amabilidad.', 1, 1, 0, 1, 0)
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (2, N'Claridad de la información entregada por su ejecutiva.', 2, 1, 0, 0, 0)
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (3, N'Oportunidad en la entrega de información respecto al avance del proceso', 3, 1, 0, 0, 0)
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (4, N'Buena disposición', 1, 2, 0, 1, 0)
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (5, N'¿Como califica usted el servicio de su ejecutiva?', 2, 2, 0, 0, 0)
GO
INSERT [dbo].[Preguntas] ([Id], [Glosa], [Orden], [EncuestaId], [TipoPregunta], [EsPreguntaSnex], [Deshabilitado]) VALUES (6, N'Recomendaría a algún familair a Bci Seguros', 3, 2, 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Preguntas] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON 
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [EsAdministrador], [IdentificadorExterno], [Contrasena], [Correo], [Rut], [SeccionId], [Deshabilitado]) VALUES (1, N'José Calderón', 1, NULL, N'jose', N'jose@pulse-it.cl', 22328684, 1, 0)
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [EsAdministrador], [IdentificadorExterno], [Contrasena], [Correo], [Rut], [SeccionId], [Deshabilitado]) VALUES (2, N'Nicolas Fernandez', 1, NULL, N'nico', N'nicolas@pulse-it.cl', 12581156, 1, 0)
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [EsAdministrador], [IdentificadorExterno], [Contrasena], [Correo], [Rut], [SeccionId], [Deshabilitado]) VALUES (3, N'Darwin Perez', 0, NULL, N'darwin', N'darwin@pulse-it.cl', 23198904, 2, 0)
GO
SET IDENTITY_INSERT [dbo].[Usuarios] OFF