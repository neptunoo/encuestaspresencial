﻿ALTER TABLE [Encuestas] ADD [EsCompaniaAlt] bit NOT NULL DEFAULT 0;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20181020001924_MultiCompania', N'2.1.2-rtm-30932');

GO

