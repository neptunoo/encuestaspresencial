﻿ALTER TABLE [Respuestas] DROP CONSTRAINT [FK_Respuestas_Clientes_ClienteId];

GO

DROP INDEX [IX_Usuarios_Rut] ON [Usuarios];

GO

DROP INDEX [IX_Respuestas_ClienteId] ON [Respuestas];

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Usuarios]') AND [c].[name] = N'IdentificadorExterno');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Usuarios] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Usuarios] DROP COLUMN [IdentificadorExterno];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Usuarios]') AND [c].[name] = N'Rut');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Usuarios] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Usuarios] DROP COLUMN [Rut];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Respuestas]') AND [c].[name] = N'FechaTermino');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Respuestas] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Respuestas] DROP COLUMN [FechaTermino];

GO

EXEC sp_rename N'[Respuestas].[ClienteId]', N'RutCliente', N'COLUMN';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20181005053810_SimplificaUsuario', N'2.1.2-rtm-30932');

GO

