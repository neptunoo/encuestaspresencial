﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace WebEncuesta.Domain.Data
{
    public static class DbContextConfigurator
    {
        public static DbContextOptionsBuilder<WebEncuestaDbContext> ConfigureForCurrentDb(this DbContextOptionsBuilder builder, string connectionString)
        {
            return (DbContextOptionsBuilder<WebEncuestaDbContext>)builder
                .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning))
                .UseSqlServer(connectionString, sqlOptions => {
                    sqlOptions.EnableRetryOnFailure();
                });
        }
    }
}
