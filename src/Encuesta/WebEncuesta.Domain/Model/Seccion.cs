﻿using System.ComponentModel.DataAnnotations;

namespace WebEncuesta.Domain.Model
{
    public class Seccion : IEntidadListable
    {
        public int Id { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public int AreaId { get; set; }
        public Area Area { get; set; }
    }
}
