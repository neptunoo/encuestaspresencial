﻿using System;

namespace WebEncuesta.Domain.Model.Auditorias
{
    public class RegistroAuditoria
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public int EncuestaId { get; set; }
        public string ValorOriginal { get; set; }
        public string ValorNuevo { get; set; }
        public string Mensaje { get; set; }
        public string QueCambio { get; set; }
    }
}
