﻿namespace WebEncuesta.Domain.Model
{
    public class Area : IEntidadListable
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

    }
}