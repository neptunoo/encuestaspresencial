﻿namespace WebEncuesta.Domain.Model
{
    public class Usuario : IEntidad
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool EsAdministrador { get; set; }
        public string Contrasena { get; set; }
        public string Correo { get; set; }
        public Seccion Seccion { get; set; }
        public int? SeccionId { get; set; }
        public bool Deshabilitado { get; set; }
    }
}
