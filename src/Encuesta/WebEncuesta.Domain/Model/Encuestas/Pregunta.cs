﻿namespace WebEncuesta.Domain.Model.Encuestas
{
    public class Pregunta
    {
        public int Id { get; set; }
        public string Glosa { get; set; }
        public int Orden { get; set; }
        public int EncuestaId { get; set; }
        public Encuesta Encuesta { get; set; }
        public TipoPregunta TipoPregunta { get; set; }
        public bool EsPreguntaSnex { get; set; }
        public bool Deshabilitado { get; set; }
    }
}
