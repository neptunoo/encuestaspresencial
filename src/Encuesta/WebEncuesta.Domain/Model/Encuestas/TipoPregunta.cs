﻿namespace WebEncuesta.Domain.Model.Encuestas
{
    public enum TipoPregunta
    {
        Caritas3,
        Caritas5,
        Texto,
        TextoNumerico,
        Radios,
    }
}
