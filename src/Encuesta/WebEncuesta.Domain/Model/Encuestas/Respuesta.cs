﻿using System;
using System.Collections.ObjectModel;

namespace WebEncuesta.Domain.Model.Encuestas
{
    public class Respuesta
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public Collection<RespuestaPregunta> Respuestas { get; set; }
        public int? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public int? RutCliente { get; set; }
        public int? EncuestaId { get; set; }
        public Encuesta Encuesta { get; set; }
    }
}
/*
Como crear un migration
1. Realizar cambios en los modelos, aqui ya hice los cambios previamente
2. Setear el proyecto migrator como startup project
3.  Agregar el migration con el proyecto mencionado
 */