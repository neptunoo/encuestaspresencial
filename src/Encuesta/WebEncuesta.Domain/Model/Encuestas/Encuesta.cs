﻿using System.Collections.ObjectModel;

namespace WebEncuesta.Domain.Model.Encuestas
{
    public class Encuesta
    {
        public Encuesta()
        {
            Vigencia = new RangoFecha();
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public Collection<Pregunta> Preguntas { get; set; }
        public RangoFecha Vigencia { get; set; }
        public int? SeccionId { get; set; }
        public Seccion Seccion { get; set; }
        public bool Deshabilitada { get; set; }
        public bool EsCompaniaAlt { get; set; }

        public override string ToString()
        {
            return
                $"{Titulo} vigente entre el {Vigencia?.Desde?.ToString("dd/MM/yyyy")} hasta {Vigencia?.Hasta?.ToString("dd/MM/yyyy")}";
        }
    }
}
