﻿namespace WebEncuesta.Domain.Model.Encuestas
{
    public class RespuestaPregunta
    {
        public int Id { get; set; }
        public int RespuestaId { get; set; }
        public Respuesta Respuesta { get; set; }
        public int PreguntaId { get; set; }
        public Pregunta Pregunta { get; set; }
        public int? Valor { get; set; }
        public bool FuePreguntaSnex { get; set; }
    }
}
