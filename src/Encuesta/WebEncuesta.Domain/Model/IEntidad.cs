﻿namespace WebEncuesta.Domain.Model
{
    public interface IEntidad
    {
        int Id { get; set; }
    }
}
