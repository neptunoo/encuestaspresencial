﻿namespace WebEncuesta.Domain.Model.Tipos
{
    public enum TiposMes
    {
        Indefinido,
        Enero,
        Febrero,
        Marzo,
        Abril,
        Mayo,
        Junio,
        Julio,
        Agosto,
        Setiembre,
        Octubre,
        Noviembre,
        Diciembre,
    }
}
