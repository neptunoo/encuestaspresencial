﻿using System;
using Microsoft.EntityFrameworkCore;

namespace WebEncuesta.Domain.Model
{
    [Owned]
    public class RangoFecha
    {
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
    }
}
