﻿namespace WebEncuesta.Domain.Model
{
    public class Cliente
    {
        public int Id { get; set; }
        public int Rut { get; set; }
    }
}
