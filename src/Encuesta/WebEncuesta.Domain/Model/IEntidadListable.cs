﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebEncuesta.Domain.Model
{
    public interface IEntidadListable : IEntidad
    {
        string Nombre { get; set; }
    }
}
