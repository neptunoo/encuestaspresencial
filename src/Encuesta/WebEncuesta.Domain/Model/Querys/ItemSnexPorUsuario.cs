﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WebEncuesta.Domain.Model.Tipos;

namespace WebEncuesta.Domain.Model.Querys
{
    public class ItemSnexPorUsuario
    {
        public string AreaNombre { get; set; }
        public string SeccionNombre { get; set; }
        public string UsuarioNombre { get; set; }
        public int Ano { get; set; }
        public int Mes { get; set; }
        public int Positivos { get; set; }
        public int Negativos { get; set; }
        public int Total { get; set; }
        [NotMapped]
        public float? Snex { get; set; }

        [NotMapped]
        public string EtiquetaMes { get; set; }
    }
}
