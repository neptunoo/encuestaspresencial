using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using WebEncuesta.Domain.Data;
using WebEncuesta.StartupExtensions;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace WebEncuesta
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
                {
                    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie();

            services
            .AddEntityFramework(Configuration)
            .AddAutoMapper()
            .AddMvcWithRazorOptions();
            services.AddApplicationInsightsTelemetry(Configuration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseDeveloperExceptionPage();

            //app.UseHttpsRedirection();

            var supportedCultures = new[]
            {
                new CultureInfo("en"),
                new CultureInfo("en-US"),
                new CultureInfo("es"),
                new CultureInfo("es-Cl")
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("es-Cl"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });


            //RunMigration(serviceProvider);

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseMvc();
        }

        private static void RunMigration(IServiceProvider serviceProvider)
        {
            if (Environment.GetEnvironmentVariable("bypassMigration") != null)
                return;

            var context = serviceProvider.GetService<WebEncuestaDbContext>();
            context.RunMigration();
        }
    }
}
