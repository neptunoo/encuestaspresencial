﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebEncuesta.Helpers.Validations
{
    public class NombreAttribute : RegularExpressionAttribute, IClientModelValidator
    {
        private static string _expression = "[áéíóúÁÉÍÓÚÑñA-Za-z ]*";
        private static string _mensajeError = "No válido";

        public NombreAttribute() : base(_expression)
        {
            ErrorMessage = _mensajeError;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            ValidationHelper.MergeAttribute(context.Attributes, "data-val", "true");
            ValidationHelper.MergeAttribute(context.Attributes, "data-val-nombre", "true");
            ValidationHelper.MergeAttribute(context.Attributes, "data-val-regex-pattern", _expression);
            ValidationHelper.MergeAttribute(context.Attributes, "data-val-regex", _mensajeError);
        }
    }
}
