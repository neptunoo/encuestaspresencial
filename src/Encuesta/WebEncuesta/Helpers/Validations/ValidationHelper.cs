﻿using System.Collections.Generic;

namespace WebEncuesta.Helpers.Validations
{
    public class ValidationHelper
    {
        public const string MensajeRequerido = "Campo requerido";
        public static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key))
            {
                return false;
            }

            attributes.Add(key, value);
            return true;
        }
    }
}
