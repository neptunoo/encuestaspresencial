﻿using System;

namespace WebEncuesta.Helpers
{
    public class PresentadoresHelpers
    {
        public static readonly string MessageToken = "Message";
        public static readonly string SavedMessage = "Guardado con éxito";

        public static string DigitoVerificador(int rut)
        {
            double T = rut;
            double M = 0, S = 1;
            while (T != 0)
            {
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
                T = Math.Floor(T / 10);
            }
            return S != 0 ?  $"{(S - 1)}" : "K";
        }

        public static string Rut(int rut)
        {
            return $"{Formatear(rut.ToString())}-{DigitoVerificador(rut)}";
        }

        public static bool RutFormateadoEsValido(string rut)
        {
            rut = rut?.ToUpper();
            return string.IsNullOrEmpty(rut) || rut.EndsWith(DigitoVerificador(ExtraerRutDeRutFormateado(rut).GetValueOrDefault()));
        }

        public static int? ExtraerRutDeRutFormateado(string rutFormateado)
        {
            if (string.IsNullOrEmpty(rutFormateado)) return null;

            rutFormateado = rutFormateado.ToUpper();
            rutFormateado = rutFormateado.Replace(",", "");
            rutFormateado = rutFormateado.Replace(".", "");
            rutFormateado = rutFormateado.Replace("-", "");

            if (int.TryParse(rutFormateado.Substring(0, rutFormateado.Length - 1), out var resultado))
                return resultado;

            return null;
        }

        public static string Formatear(string rut)
        {
            var cont = 0;
            var format = string.Empty;
            if (rut.Length == 0) return format;
            for (var i = rut.Length -1; i >= 0; i--)
            {
                format = rut.Substring(i, 1) + format;
                cont++;
                if (cont != 3 || i == 0) continue;
                format = "." + format;
                cont = 0;
            }
            return format;
        }
    }
}