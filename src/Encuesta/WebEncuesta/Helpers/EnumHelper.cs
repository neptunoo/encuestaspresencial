﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace WebEncuesta.Helpers
{
    public static class EnumHelper
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            var enumType = enumValue.GetType();
            var names = new List<string>();
            foreach (var e in Enum.GetValues(enumType))
            {
                var flag = (Enum)e;
                if (enumValue.HasFlag(flag))
                {
                    names.Add(GetSingleDisplayName(flag));
                }
            }
            if (names.Count <= 0) throw new ArgumentException();
            if (names.Count == 1) return names.First();
            return string.Join(",", names);
        }
       
        public static string GetSingleDisplayName(this Enum flag)
        {
            try
            {
                return flag.GetType()
                    .GetMember(flag.ToString())
                    .First()
                    .GetCustomAttribute<DisplayAttribute>()
                    .Name;
            }
            catch
            {
                return flag.ToString();
            }
        }
    }
}
