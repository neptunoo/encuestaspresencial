﻿using System;
using AutoMapper;
using WebEncuesta.Domain.Model;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.Helpers.Mappers;

namespace WebEncuesta.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateGenericMaps();

            CreateMap<Usuario, ViewModels.UsuarioViewModel>()
               .ReverseMap()
               .ForMember(dest => dest.Seccion, opt => opt.Ignore());

            CreateMap<Encuesta, ViewModels.EncuestaViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.Seccion, opt => opt.Ignore());

            CreateMap<Pregunta, ViewModels.PreguntaViewModel>().ReverseMap();
            CreateMap<Respuesta, ViewModels.RespuestaViewModel>()
                .ForMember(dest => dest.RutCliente, opt => opt.MapFrom(src => src.RutCliente.HasValue ? PresentadoresHelpers.Rut(src.RutCliente.Value) : string.Empty));
        }

        private void CreateGenericMaps()
        {
            CreateMap<DateTime, string>().ConvertUsing(new DateTimeToString());
            CreateMap<DateTime?, string>().ConvertUsing(new DateTimeNullableToString());
            CreateMap<string, DateTime>().ConvertUsing(new StringToDateTime());
            CreateMap<string, DateTime?>().ConvertUsing(new StringToNullableDateTime());
        }
    }
}