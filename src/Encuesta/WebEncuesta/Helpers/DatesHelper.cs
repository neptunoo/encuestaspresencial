﻿using System;

namespace WebEncuesta.Helpers
{
    public  class DatesHelper
    {
        public static int Edad(DateTime? fechaNacimiento)
        {
            if (!fechaNacimiento.HasValue) return 0;
            var birthDate = fechaNacimiento.Value;
            var now = DateTime.Now;
            var age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age;
        }
    }
}
