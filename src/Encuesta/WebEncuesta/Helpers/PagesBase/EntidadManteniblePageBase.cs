﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.Helpers.PagesBase
{
    public class EntidadManteniblePageBase<TM> : PageModel
        where TM : class, IEntidad, new()
    {
        protected readonly WebEncuestaDbContext _db;
        protected readonly IMapper _mapper;

        public EntidadManteniblePageBase(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<List<TM>> MapCurrentData()
        {
            var data = await _db.Set<TM>().OrderByDescending(t => t.Id).ToListAsync();
            return data;
        }

        public async Task<List<TM>> MapCurrentData(params Expression<Func<TM, object>>[] navigationPropertyPaths)
        {
            var set = _db.Set<TM>().AsNoTracking().AsQueryable();
            foreach (var each in navigationPropertyPaths)
                set = set.Include(each);
            return await set.ToListAsync();
        }
        

        public async Task AddEntityAsync(TM vm)
        {
            await _db.Set<TM>().AddAsync(vm);
            await _db.SaveChangesAsync();
            TempData[PresentadoresHelpers.MessageToken] = PresentadoresHelpers.SavedMessage;
        }
    }
}
