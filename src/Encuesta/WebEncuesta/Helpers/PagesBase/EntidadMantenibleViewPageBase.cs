﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.Helpers.PagesBase
{
    public class EntidadMantenibleViewPageBase<TVm, TM> : PageModel
        where TVm : IEntidad
        where TM : class, IEntidad, new()
    {
        protected readonly WebEncuestaDbContext _db;
        protected readonly IMapper _mapper;

        public EntidadMantenibleViewPageBase(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<List<TVm>> MapCurrentData()
        {
            var data = await _db.Set<TM>().OrderByDescending(t => t.Id).ToListAsync();
            return _mapper.Map<List<TVm>>(data);
        }

        public async Task<List<TVm>> MapCurrentData(params Expression<Func<TM, object>>[] navigationPropertyPaths)
        {
            var set = _db.Set<TM>().AsNoTracking().AsQueryable();
            foreach (var each in navigationPropertyPaths)
                set = set.Include(each);
            return _mapper.Map<List<TVm>>(await set.ToListAsync());
        }


        public async Task AddEntityAsync(TVm vm)
        {
            var entityToAdd = new TM();
            _mapper.Map(vm, entityToAdd);
            await _db.Set<TM>().AddAsync(entityToAdd);
            await _db.SaveChangesAsync();
            TempData[PresentadoresHelpers.MessageToken] = PresentadoresHelpers.SavedMessage;
        }

        public async Task UpdateEntityAsync(TVm vm)
        {
            var entityToModify = await _db.Set<TM>().FindAsync(vm.Id);
            _mapper.Map(vm, entityToModify);
            await _db.SaveChangesAsync();
            TempData[PresentadoresHelpers.MessageToken] = PresentadoresHelpers.SavedMessage;
        }
    }
}
