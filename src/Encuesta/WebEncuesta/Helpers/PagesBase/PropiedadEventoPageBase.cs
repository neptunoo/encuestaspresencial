﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.Helpers.PagesBase
{
    public class PropiedadEventoPageBase<TVm, TM> : PageModel
        where TVm : IEntidad
        where TM : class, IEntidad, new()
    {
        protected readonly WebEncuestaDbContext Db;
        protected readonly IMapper Mapper;

        public PropiedadEventoPageBase(WebEncuestaDbContext db, IMapper mapper)
        {
            Db = db;
            Mapper = mapper;
        }

        public async Task<TVm> MapEntityCreatingIfNotExistsAsync(int parentId)
        {
            //if (await Db.Set<TM>().AsNoTracking().AnyAsync(t => t.EventoId == parentId))
            //{
            //    var current = await Db.Set<TM>().AsNoTracking().SingleAsync(t => t.EventoId == parentId);
            //    return Mapper.Map<TVm>(current);
            //}
            var entityToAdd = new TM();
            await Db.Set<TM>().AddAsync(entityToAdd);
            await Db.SaveChangesAsync();
            return Mapper.Map<TVm>(entityToAdd);
        }

        public async Task UpdateEntityAsync(int entityId, TVm vm)
        {
            var current = await Db.Set<TM>().FindAsync(entityId);
            Mapper.Map(vm, current);
            await Db.SaveChangesAsync();
            Mensajear();
        }
        public async Task UpdateEntityAsync(TVm vm)
        {
            var current = await Db.Set<TM>().FindAsync(vm.Id);
            Mapper.Map(vm, current);
            await Db.SaveChangesAsync();
            Mensajear();
        }

        protected void Mensajear()
        {
            TempData[PresentadoresHelpers.MessageToken] = PresentadoresHelpers.SavedMessage;
        }

    }
}
