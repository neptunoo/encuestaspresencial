﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.Helpers.Extensions
{
    public static class DbSetListExtensions
    {
        public static List<SelectListItem> ASelectList<T>(this List<T> list) where T : IEntidadListable
        {
            return list.Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = n.Nombre
            }).ToList();
        }

        public static List<SelectListItem> ASelectListConOpcionTodos<T>(this List<T> list) where T : IEntidadListable
        {
            var result = list.ASelectList();
            result.Insert(0, new SelectListItem("Todos", "0"));
            return result;
        }
    }
}
