﻿using System;
using System.Security.Claims;

namespace WebEncuesta.Helpers.Extensions
{
    public static class ApplicationClaimExtensions
    {
        public const string UsuarioIdClaimSchemaType = "usuarioid";

        public static string GetEmail(this ClaimsPrincipal principal) => principal.FindFirstValue(ClaimTypes.NameIdentifier);
        public static int GetUsuarioId(this ClaimsPrincipal principal) => Convert.ToInt32(principal.FindFirstValue(UsuarioIdClaimSchemaType));
        public static string GetName(this ClaimsPrincipal principal) => principal.FindFirstValue(ClaimTypes.Name);
    }
}
