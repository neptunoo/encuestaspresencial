﻿using System;
using System.Linq;
using AutoMapper;

namespace WebEncuesta.Helpers.Mappers
{
    public class StringToDateTime : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string source, DateTime destination, ResolutionContext context)
        {
            var dateToken = source.Split(' ').First();
            var timeToken = source.Split(' ').Last();

            var datesParts = dateToken.Contains("/") ? dateToken.Split('/') : dateToken.Split('-');

            if (timeToken.Contains(":"))
            {
                var timeParts = timeToken.Split(':');
                return new DateTime(int.Parse(datesParts.Last()),
                    int.Parse(datesParts[1]),
                    int.Parse(datesParts.First()),
                    int.Parse(timeParts.First()),
                    int.Parse(timeParts[1]),
                    0);
            }
            return new DateTime(int.Parse(datesParts.Last()),
                                int.Parse(datesParts[1]),
                                int.Parse(datesParts.First()));
        }
    }
}
