﻿using System;
using AutoMapper;

namespace WebEncuesta.Helpers.Mappers
{
    public class DateTimeNullableToString : ITypeConverter<DateTime?, string>
    {
        public string Convert(DateTime? source, string destination, ResolutionContext context)
        {
            return source.HasValue
                ? $"{source.Value.Day.ToString().PadLeft(2, '0')}-{source.Value.Month.ToString().PadLeft(2, '0')}-{source.Value.Year} {source.Value.Hour}:{source.Value.Minute.ToString().PadLeft(2, '0')}"
                : null;
        }
    }

   
}
