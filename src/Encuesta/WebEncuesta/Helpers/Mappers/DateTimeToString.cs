﻿using System;
using AutoMapper;

namespace WebEncuesta.Helpers.Mappers
{
    public class DateTimeToString : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return $"{source.Day.ToString().PadLeft(2, '0')}-{source.Month.ToString().PadLeft(2, '0')}-{source.Year} {source.Hour}:{source.Minute}";
        }
    }
}
