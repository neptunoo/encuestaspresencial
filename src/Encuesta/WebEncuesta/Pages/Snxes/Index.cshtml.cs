﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.Domain.Model.Tipos;
using WebEncuesta.Helpers.Extensions;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Snxes
{
    public class IndexModel : PageModel
    {
        protected readonly WebEncuestaDbContext _db;
        protected readonly IMapper _mapper;
        [BindProperty] public ExportarViewModel Vm { get; set; }
        public List<SelectListItem> Areas { get; set; }
        public IndexModel(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public List<ItemSnexViewModel> VmList { get; set; }
        public async Task OnGet()
        {

            Vm = new ExportarViewModel();
            Areas = (await _db.Areas.AsNoTracking().ToListAsync()).ASelectList();
            //await Rebind();
        }

        private async Task Rebind()
        {
            VmList = new List<ItemSnexViewModel>();

            var respuestas = await _db.Respuestas.Include(t => t.Encuesta).ThenInclude(t => t.Seccion).AsNoTracking()
                .Include(t => t.Respuestas).ToListAsync();

            if (Vm.SeccionAreaId.GetValueOrDefault() > 0)
                respuestas = respuestas.Where(t => t.Encuesta.Seccion.AreaId == Vm.SeccionAreaId).ToList();

            if (Vm.SeccionId.GetValueOrDefault() > 0)
                respuestas = respuestas.Where(t => t.Encuesta.SeccionId == Vm.SeccionId).ToList();

            if (Vm.EjecutivoId.GetValueOrDefault() > 0)
                respuestas = respuestas.Where(t => t.Encuesta.SeccionId == Vm.SeccionId).ToList();


            var periodos = respuestas.Select(t => new DateTime(t.Fecha.Year, t.Fecha.Month, 1)).Distinct().ToList()
                .OrderBy(t => t);
            foreach (var cadaPeriodo in periodos)
            {
                var i = new ItemSnexViewModel {Mes = $"{(TiposMes) cadaPeriodo.Month}-{cadaPeriodo:yy}"};

                var respuestasPeriodo = respuestas
                    .Where(t => t.Fecha.Year == cadaPeriodo.Year && t.Fecha.Month == cadaPeriodo.Month).ToList();
                i.Encuestas = respuestasPeriodo.Count();

                List<RespuestaPregunta> respuestasPreguntasSnex = new List<RespuestaPregunta>();
                foreach (var cadaRespuestaPeriodo in respuestasPeriodo)
                {
                    var respuestaSnex = cadaRespuestaPeriodo.Respuestas.FirstOrDefault(t => t.FuePreguntaSnex);
                    respuestasPreguntasSnex.Add(respuestaSnex);
                    i.Respuestas += cadaRespuestaPeriodo.Respuestas.Count;
                }

                float respuestasPositivas = respuestasPreguntasSnex.Count(t => t.Valor == 1);
                float respuestasNegativas = respuestasPreguntasSnex.Count(t => t.Valor == 3);

                i.ValorSnex = (respuestasPositivas - respuestasNegativas) / (float) i.Encuestas;

                VmList.Add(i);
            }
        }

        public async Task<IActionResult> OnPost()
        {
            await Rebind();
            return Page();
        }

        private List<Seccion> SeccionesSeleccionadas(Area eachArea)
        {
            if (Vm.SeccionId > 0)
                return _db.Secciones.AsNoTracking().Where(t => t.Id == Vm.SeccionId).ToList();
            return _db.Secciones.AsNoTracking().Where(t => t.AreaId == eachArea.Id).ToList();
        }

        private List<Area> AreasSeleccionadas()
        {
            if (Vm.SeccionAreaId > 0)
                return _db.Areas.AsNoTracking().Where(t => t.Id == Vm.SeccionAreaId).ToList();
            return _db.Areas.AsNoTracking().ToList();
        }
    }
}