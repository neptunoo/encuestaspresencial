﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Respuestas
{
    public class VerModel : PageModel
    {

        protected readonly WebEncuestaDbContext _db;
        protected readonly IMapper _mapper;

        public VerModel(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [BindProperty]
        public EncuestaViewModel Vm { get; set; }



        public async Task OnGet(int id)
        {
            var respuesta = _db.Respuestas.Find(id);
            var data = await _db.Encuestas.Include(i => i.Preguntas).FirstOrDefaultAsync(t => t.Id == respuesta.EncuestaId);
            Vm = _mapper.Map<EncuestaViewModel>(data);

            var respuestasPreguntas = _db.RespuestasPreguntas.Where(t => t.RespuestaId == id).ToList();
            foreach (var item in Vm.Preguntas)
            {
                if (respuestasPreguntas.Any(t => t.PreguntaId == item.Id))
                {
                    var respuestaPregunta = respuestasPreguntas.First(t => t.PreguntaId == item.Id);
                    item.Valor = respuestaPregunta.Valor.GetValueOrDefault().ToString();
                }
            }
        }
    }
}