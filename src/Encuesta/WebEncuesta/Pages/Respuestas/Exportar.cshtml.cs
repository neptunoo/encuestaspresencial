﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers;
using WebEncuesta.Helpers.Extensions;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Respuestas
{
    public class ExportarModel : PageModel
    {
        private readonly WebEncuestaDbContext _db;
        [BindProperty] public ExportarViewModel Vm { get; set; }
        public List<SelectListItem> Areas { get; set; }
        public ExportarModel(WebEncuestaDbContext db)
        {
            _db = db;
        }
        public async Task OnGet()
        {
            Vm = new ExportarViewModel();
            Areas = (await _db.Areas.AsNoTracking().ToListAsync()).ASelectList();
        }



        public async Task<IActionResult> OnPost()
        {
            string sFileName = $"BciSegurosAEP_{DateTime.Now.Ticks}.xlsx";
            var memory = new MemoryStream();
            using (var fs = new MemoryStream())
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();

                ISheet excelSheet = workbook.CreateSheet($"Resultados");
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("Area");
                row.CreateCell(1).SetCellValue("Seccion");
                row.CreateCell(2).SetCellValue("Fecha");
                row.CreateCell(3).SetCellValue("Encuesta");
                row.CreateCell(4).SetCellValue("Ejecutivo");
                row.CreateCell(5).SetCellValue("Cliente");

                var cantidadMaximaPreguntas = ObtenerMaximoPreguntas(_db);
                int numeroCelda = 6;
                for (int i = 0; i < cantidadMaximaPreguntas; i++)
                    row.CreateCell(numeroCelda++).SetCellValue($"Pregunta {i + 1}");

                int numeroFila = 1;
                foreach (var eachArea in AreasSeleccionadas())
                {
                    foreach (var eachSeccion in SeccionesSeleccionadas(eachArea))
                    {
                        foreach (var eachEncuestaSeccion in _db.Encuestas.AsNoTracking().Where(t => t.SeccionId == eachSeccion.Id).OrderByDescending(o => o.Vigencia.Hasta).ToList())
                        {
                            if (_db.Respuestas.AsNoTracking().Any(t => t.EncuestaId == eachEncuestaSeccion.Id))
                            {
                                var encuesta = _db.Encuestas.AsNoTracking().Include(i => i.Preguntas).Single(t => t.Id == eachEncuestaSeccion.Id);
                                var respuestas = _db.Respuestas.AsNoTracking().Include(i => i.Respuestas).Include(i => i.Usuario).Where(t => t.EncuestaId == eachEncuestaSeccion.Id).OrderByDescending(o => o.Fecha).ToList();

                                if (Vm.VigenciaDesde.HasValue)
                                    respuestas = respuestas.Where(t => t.Fecha >= Vm.VigenciaDesde).ToList();

                                if (Vm.VigenciaHasta.HasValue)
                                    respuestas = respuestas.Where(t => t.Fecha <= Vm.VigenciaHasta).ToList();


                                foreach (var cadaRespuesta in respuestas)
                                {
                                    row = excelSheet.CreateRow(numeroFila++);
                                    row.CreateCell(0).SetCellValue(eachArea.Nombre);
                                    row.CreateCell(1).SetCellValue(eachSeccion.Nombre);
                                    row.CreateCell(2).SetCellValue($"{cadaRespuesta.Fecha:dd-MM-yyyy} {cadaRespuesta.Fecha.ToShortTimeString()}");
                                    row.CreateCell(3).SetCellValue(encuesta.Titulo);
                                    row.CreateCell(4).SetCellValue(cadaRespuesta.Usuario?.Nombre);
                                    row.CreateCell(5).SetCellValue(PresentadoresHelpers.Rut(cadaRespuesta.RutCliente.GetValueOrDefault(0)));
                                    numeroCelda = 6;

                                    foreach (var cadaPregunta in encuesta.Preguntas.OrderBy(o => o.Orden))
                                    {
                                        var respuestaCadaPregunta = cadaRespuesta.Respuestas.FirstOrDefault(t => t.PreguntaId == cadaPregunta.Id);
                                        if (respuestaCadaPregunta != null)
                                        {
                                            var valorDecodeado = DecodeValor(respuestaCadaPregunta.Valor);
                                            row.CreateCell(numeroCelda++).SetCellValue($"{valorDecodeado}");
                                        }
                                        else
                                            numeroCelda++;
                                    }
                                }
                            }
                        }
                    }
                }

                workbook.Write(fs);
                var byteArray = fs.ToArray();
                memory.Write(byteArray, 0, byteArray.Length);
            }
            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }

        private List<Seccion> SeccionesSeleccionadas(Area eachArea)
        {
            if (Vm.SeccionId > 0)
                return _db.Secciones.AsNoTracking().Where(t => t.Id == Vm.SeccionId).ToList();
            return _db.Secciones.AsNoTracking().Where(t => t.AreaId == eachArea.Id).ToList();
        }

        private List<Area> AreasSeleccionadas()
        {
            if (Vm.SeccionAreaId > 0)
                return _db.Areas.AsNoTracking().Where(t => t.Id == Vm.SeccionAreaId).ToList();
            return _db.Areas.AsNoTracking().ToList();
        }

        private int ObtenerMaximoPreguntas(DbContext db)
        {
            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "select MAX(p.cnt) from (select  EncuestaId, COUNT(1) cnt from Preguntas group by EncuestaId) as p";
                db.Database.OpenConnection();
                using (var resultSequence = command.ExecuteReader())
                {
                    if (resultSequence.Read())
                    {
                        return Convert.ToInt32(resultSequence[0]);
                    }
                    throw new InvalidOperationException("Sequence in database is not created");
                }
            }
        }

        private string DecodeValor(int? valor)
        {
            if (!valor.HasValue) return string.Empty;
            switch (valor.Value)
            {
                case 1: return "Bueno";
                case 2: return "Neutral";
                case 3: return "Malo";
                default: return string.Empty;
            }
        }

        public async Task<JsonResult> OnGetSecciones(int areaId)
        {
            return new JsonResult((await _db.Secciones.AsNoTracking().Where(t => t.AreaId == areaId).ToListAsync())
                .ASelectListConOpcionTodos());
        }
    }
}