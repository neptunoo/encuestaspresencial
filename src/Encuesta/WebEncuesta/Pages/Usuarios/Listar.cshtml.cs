﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers.Extensions;
using WebEncuesta.Helpers.PagesBase;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Usuarios
{
    public class ListarModel : EntidadMantenibleViewPageBase<UsuarioViewModel, Usuario>
    {

        public ListarModel(WebEncuestaDbContext db, IMapper mapper) : base(db, mapper) { }

        [BindProperty]
        public UsuarioViewModel Vm { get; set; }

        public List<UsuarioViewModel> VmList { get; set; }

        public List<SelectListItem> Areas { get; set; }

        public async Task OnGet()
        {
            Vm = new UsuarioViewModel();
            VmList = await MapCurrentData(t => t.Seccion, p => p.Seccion.Area);
            Areas = (await _db.Areas.AsNoTracking().ToListAsync()).ASelectList();
        }

        public async Task<JsonResult> OnGetSecciones(int areaId)
        {
            return new JsonResult((await _db.Secciones.AsNoTracking().Where(t => t.AreaId == areaId).ToListAsync())
                .ASelectList());
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            if (Vm.Id > 0)
                await UpdateEntityAsync(Vm);
            else
                await AddEntityAsync(Vm);

            return RedirectToPage();
        }
    }
}