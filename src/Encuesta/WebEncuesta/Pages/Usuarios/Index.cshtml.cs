﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers.PagesBase;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Usuarios
{
    public class IndexModel : EntidadMantenibleViewPageBase<UsuarioViewModel, Usuario>
    {
        public IndexModel(WebEncuestaDbContext db, IMapper mapper) : base(db, mapper) { }

        [BindProperty]
        public UsuarioViewModel Vm { get; set; }

        public List<UsuarioViewModel> VmList { get; set; }
        public List<SelectListItem> Secciones { get; set; }

        public async Task OnGet()
        {
            Vm = new UsuarioViewModel();
            VmList = await MapCurrentData(t => t.Seccion);
            Secciones = _db.Secciones.AsNoTracking().Include(t => t.Area).Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = $"[{n.Area.Nombre}] {n.Nombre}"
            }).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            await AddEntityAsync(Vm);
            return RedirectToPage();
        }
    }
}