﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Encuestas
{
    public class IndexModel : PageModel
    {
        private readonly WebEncuestaDbContext _db;
        private readonly IMapper _mapper;
        public IndexModel(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public List<EncuestaViewModel> VmList { get; set; }
        public async Task OnGet()
        {
            var data = await _db.Encuestas.Include(t => t.Seccion.Area).AsNoTracking().ToListAsync();
            VmList = _mapper.Map<List<EncuestaViewModel>>(data);
        }
    }
}