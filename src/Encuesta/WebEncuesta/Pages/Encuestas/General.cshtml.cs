﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.Helpers;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Encuestas
{
    public class GeneralModel : PageModel
    {
        private readonly WebEncuestaDbContext _db;
        private IMapper _mapper;
        public GeneralModel(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [BindProperty] public EncuestaViewModel Vm { get; set; }
        public List<SelectListItem> Secciones { get; set; }
        public async Task<IActionResult> OnGet(int id)
        {
            InicializarFormulario();

            if (id == 0)
            {
                Vm = new EncuestaViewModel { Id = 0 };
                return Page();
            }

            var current = await _db.Encuestas.AsNoTracking().SingleAsync(t => t.Id == id);
            Vm = _mapper.Map<EncuestaViewModel>(current);
            return Page();
        }

        private void InicializarFormulario()
        {
            Secciones = _db.Secciones.Include(t => t.Area).AsNoTracking().Select(n => new SelectListItem { Value = n.Id.ToString(), Text = $"[ {n.Area.Nombre} ] {n.Nombre}" }).ToList();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid) return Page();

            if (await EvaluarConflictoVigenciaReportandolo())
            {
                InicializarFormulario();
                return Page();
            }

            var encuesta = await CreateOrUpdate();
            await _db.SaveChangesAsync();
            TempData[PresentadoresHelpers.MessageToken] = PresentadoresHelpers.SavedMessage;
            InicializarFormulario();

            if (Vm.Id == 0)
                return RedirectToPage("Preguntas", new { id = encuesta.Id });
            return Page();
        }

        private async Task<bool> EvaluarConflictoVigenciaReportandolo()
        {
            if (Vm.Id == 0 && _db.Encuestas.Any(t => t.SeccionId == Vm.SeccionId &&
                                                     !t.Deshabilitada &&
                                                     (Vm.VigenciaDesde < t.Vigencia.Hasta && t.Vigencia.Desde < Vm.VigenciaHasta)))
            {
                var encuestasConflicto = await _db.Encuestas.AsNoTracking().Where(t => t.SeccionId == Vm.SeccionId &&
                                                                                       !t.Deshabilitada &&
                                                                                       (Vm.VigenciaDesde < t.Vigencia.Hasta && t.Vigencia.Desde < Vm.VigenciaHasta)).ToListAsync();
                AgregarErrores(encuestasConflicto);
                return true;
            }

            if (Vm.Id > 0 && _db.Encuestas.Any(t => t.Id != Vm.Id && t.SeccionId == Vm.SeccionId &&
                                                    !t.Deshabilitada &&
                                                    (Vm.VigenciaDesde < t.Vigencia.Hasta && t.Vigencia.Desde < Vm.VigenciaHasta)))
            {
                var encuestasConflicto = await _db.Encuestas.AsNoTracking().Where(t => t.Id != Vm.Id && t.SeccionId == Vm.SeccionId &&
                                                                                       !t.Deshabilitada &&
                                                                                       (Vm.VigenciaDesde < t.Vigencia.Hasta && t.Vigencia.Desde < Vm.VigenciaHasta)).ToListAsync();
                AgregarErrores(encuestasConflicto);
                return true;
            }

            return false;
        }

        private void AgregarErrores(List<Encuesta> encuestasConflicto)
        {
            ModelState.AddModelError("",
                encuestasConflicto.Count == 1
                    ? $"Hay un conflicto de fechas de vigencia con la encuesta: [ {encuestasConflicto[0]} ]"
                    : $"Hay conflictos de fechas de vigencia con las encuestas: [ {string.Join(", ", encuestasConflicto.Select(t => t.ToString()).ToArray())} ]");
        }

        private async Task<Encuesta> CreateOrUpdate()
        {
            if (Vm.Id == 0)
            {
                var current = _mapper.Map<Encuesta>(Vm);
                await _db.Encuestas.AddAsync(current);
                return current;
            }
            else
            {
                var current = await _db.Encuestas.FindAsync(Vm.Id);
                _mapper.Map(Vm, current);
                return current;
            }
        }
    }
}