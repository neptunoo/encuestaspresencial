﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers.PagesBase;

namespace WebEncuesta.Pages.Areas
{
    public class IndexModel : EntidadManteniblePageBase<Area>
    {
        public IndexModel(WebEncuestaDbContext db, IMapper mapper) : base(db, mapper) { }

        [BindProperty]
        public Area Vm { get; set; }

        public List<Area> VmList { get; set; }

        public async Task OnGet()
        {
            Vm = new Area();
            VmList = await MapCurrentData();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            await AddEntityAsync(Vm);
            return RedirectToPage();
        }
    }
}