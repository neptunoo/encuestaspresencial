﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers.PagesBase;

namespace WebEncuesta.Pages.Secciones
{
    public class IndexModel : EntidadManteniblePageBase<Seccion>
    {
        public IndexModel(WebEncuestaDbContext db, IMapper mapper) : base(db, mapper) { }

        [BindProperty]
        public Seccion Vm { get; set; }

        public List<Seccion> VmList { get; set; }
        public List<SelectListItem> Areas { get; set; }
        public async Task OnGet()
        {
            Vm = new Seccion();
            VmList = await MapCurrentData(t => t.Area);
            Areas = _db.Areas.AsNoTracking().Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = n.Nombre
            }).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            await AddEntityAsync(Vm);
            return RedirectToPage();
        }
    }
}