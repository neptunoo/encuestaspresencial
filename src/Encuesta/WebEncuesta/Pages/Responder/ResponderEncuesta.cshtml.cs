﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.ViewModels;
using WebEncuesta.Helpers.Extensions;

namespace WebEncuesta.Pages.Responder
{
    public class ResponderEncuestaModel : PageModel
    {
        private readonly WebEncuestaDbContext _db;
        private readonly IMapper _mapper;

        public ResponderEncuestaModel(WebEncuestaDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [BindProperty]
        public EncuestaViewModel Vm { get; set; }


        public async Task OnGet(int p)
        {
            var usuario = _db.Usuarios.First(t => t.Id == Convert.ToInt32(User.GetUsuarioId()));
            var encuesta = _db.Encuestas
                                .Where(t => t.SeccionId == usuario.SeccionId && DateTime.Now.Date >= t.Vigencia.Desde && DateTime.Now.Date <= t.Vigencia.Hasta)
                                .OrderByDescending(t => t.Vigencia.Hasta)
                                .First();

            var data = await _db.Encuestas.Include(i => i.Preguntas).FirstOrDefaultAsync(t => t.Id == encuesta.Id);
            Vm = _mapper.Map<EncuestaViewModel>(data);
            Vm.Preguntas = Vm.Preguntas.OrderBy(t => t.Orden).ToList();
        }

        public async Task<IActionResult> OnPost(int p)
        {
            Respuesta respuesta = new Respuesta
            {
                EncuestaId = Vm.Id,
                Fecha = DateTime.Now,
                UsuarioId = User.GetUsuarioId(),
                RutCliente = p,
                Respuestas = new System.Collections.ObjectModel.Collection<RespuestaPregunta>()
            };
            foreach (var item in Vm.Preguntas)
            {
                respuesta.Respuestas.Add(new RespuestaPregunta
                {
                    PreguntaId = item.Id.GetValueOrDefault(),
                    Valor = int.TryParse(item.Valor, out var parser) ? parser : 0,
                    FuePreguntaSnex = item.EsPreguntaSnex
                });
            }

            await _db.Respuestas.AddAsync(respuesta);
            await _db.SaveChangesAsync();
            return RedirectToPage("Gracias");
        }
    }
}