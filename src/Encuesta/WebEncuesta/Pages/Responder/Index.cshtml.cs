﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Razor.Language.Intermediate;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.Helpers;
using WebEncuesta.Helpers.Extensions;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Pages.Responder
{
    public class IndexModel : PageModel
    {
        private readonly WebEncuestaDbContext _db;
        private readonly ILogger<IndexModel> _logger;
        public IndexModel(WebEncuestaDbContext db, ILogger<IndexModel> logger)
        {
            _db = db;
            _logger = logger;
        }

        [BindProperty]
        public IngresoResponderEncuestaViewModel Vm { get; set; }

        public async Task OnGet()
        {
            var usuario = await _db.Usuarios.FindAsync(User.GetUsuarioId());
            var encuesta = await _db.Encuestas
                                .Where(t => t.SeccionId == usuario.SeccionId && DateTime.Now.Date >= t.Vigencia.Desde && DateTime.Now.Date <= t.Vigencia.Hasta)
                                .OrderByDescending(t => t.Vigencia.Hasta)
                                .FirstOrDefaultAsync();

            if (encuesta == null)
            {
                _logger.LogWarning($"No existen encuestas disponibles para el usuario ingresado. {User.GetUsuarioId()}");
                TempData[PresentadoresHelpers.MessageToken] = "No existen encuestas disponibles para el usuario ingresado.";
                return;
            }

            Vm = new IngresoResponderEncuestaViewModel
            {
                EsZenit = encuesta.EsCompaniaAlt,
                TituloEncuesta = encuesta.Titulo,
                DescripcionEncuesta = encuesta.Descripcion
            };
        }

        public IActionResult OnPost()
        {
            _logger.LogTrace($"Respondera la encuesta cliente {Vm.Rut}");
            return RedirectToPage("ResponderEncuesta", new { p = PresentadoresHelpers.ExtraerRutDeRutFormateado(Vm.Rut) });
        }
    }
}