﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebEncuesta.Domain.Data;

namespace WebEncuesta.StartupExtensions
{
    public static class EntityFrameworkExtensions
    {
        public static IServiceCollection AddEntityFramework(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<WebEncuestaDbContext>(options =>
            {
                options.ConfigureForCurrentDb(configuration["SQL_CONNECTION"]);
            });
        }

    }
}
