using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using WebEncuesta.Domain.Data;

namespace WebEncuesta.StartupExtensions
{
    public static class DbContextExtensions
    {
        public static IApplicationBuilder UseMigration(this IApplicationBuilder app)
        {
            return app.Use(async (context, func) =>
            {
                app.ApplicationServices.GetService<WebEncuestaDbContext>().RunMigration();
                await func();
            });
        }
    }
}