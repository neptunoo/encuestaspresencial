﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WebEncuesta.Helpers;

namespace WebEncuesta.StartupExtensions
{
    public static class AutoMapperExtensions
    {
        public static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }
    }
}
