﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace WebEncuesta.StartupExtensions
{
    public static class MvcRazorExtensions
    {
        private const string AdministrarPolicy = "Administrar";

        public static IServiceCollection AddMvcWithRazorOptions(this IServiceCollection services)
        {
            services
                .AddMvc()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AddPageRoute("/Respuestas/Index", "");
                    options.Conventions.AllowAnonymousToFolder("/Account");
                    options.Conventions.AuthorizeFolder("/Responder");
                    options.Conventions.AuthorizePage("/SignOut");
                    options.Conventions.AuthorizeFolder("/Dashboard", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Areas", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Secciones", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Ejecutivos", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Encuestas", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Respuestas", AdministrarPolicy);
                    options.Conventions.AuthorizeFolder("/Snxes", AdministrarPolicy);
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            services.AddAuthorization(options =>
            {
                options.AddPolicy(AdministrarPolicy, policy => policy.RequireRole("Admin"));
            });
            return services;
        }
    }
}
