﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeccionesController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public SeccionesController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }


        [HttpGet("{idSeccion}/Usuarios")]
        public async Task<IEnumerable<SelectListItem>> GetSecciones(int idSeccion)
        {
            return await _context.Usuarios.AsNoTracking().Where(t => t.SeccionId == idSeccion).Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = n.Nombre
            }).ToListAsync();
        }
    }
}