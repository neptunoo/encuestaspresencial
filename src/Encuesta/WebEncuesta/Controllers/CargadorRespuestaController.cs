﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Encuestas;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CargadorRespuestaController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public CargadorRespuestaController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpGet]
        public void Cargar(int cantidad)
        {
            DateTime inicio = DateTime.Now.AddDays(-cantidad);
            var encuesta = _context.Encuestas.Include(p => p.Preguntas).AsNoTracking().First(t => !t.Deshabilitada);
            var usuario = _context.Usuarios.AsNoTracking().First(t => !t.EsAdministrador);
            for (var i = 0; i < cantidad; i++)
            {
                _context.Respuestas.Add(new Respuesta
                {
                    Fecha = inicio,
                    RutCliente = 22328684,
                    UsuarioId = usuario.Id,
                    EncuestaId = encuesta.Id,
                    Respuestas = ConfigurarRespuestas(encuesta.Preguntas)
                });
                inicio = inicio.AddDays(1);
                _context.SaveChanges();
            }
        }

        private Collection<RespuestaPregunta> ConfigurarRespuestas(Collection<Pregunta> encuestaPreguntas)
        {
            var result = new Collection<RespuestaPregunta>();
            var rnd = new Random();
            foreach (var encuestaPregunta in encuestaPreguntas)
            {
                result.Add(new RespuestaPregunta
                {
                    FuePreguntaSnex = encuestaPregunta.EsPreguntaSnex,
                    Valor = rnd.Next(1, 4),
                    PreguntaId = encuestaPregunta.Id
                });
            }
            return result;
        }
    }
}