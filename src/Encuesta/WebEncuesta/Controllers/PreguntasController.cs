﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Controllers
{
    [Route("api/encuestas/{encuestaId}/[controller]")]
    [ApiController]
    public class PreguntasController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public PreguntasController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<PreguntaViewModel>> GetPreguntas(int encuestaId)
        {
            var data = await _context.Preguntas.AsNoTracking().Where(t => t.EncuestaId == encuestaId).OrderBy(t => t.Orden).ToListAsync();
            return _mapper.Map<List<PreguntaViewModel>>(data);
        }

       

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPregunta([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pregunta = await _context.Preguntas.FindAsync(id);

            if (pregunta == null)
            {
                return NotFound();
            }

            return Ok(pregunta);
        }

        [HttpGet("CambiarOrden/{tomado}/Por/{cambiado}")]
        public async Task<IActionResult> CambiarOrden(int tomado, int cambiado)
        {
            var elTomado = await _context.Preguntas.FindAsync(tomado);
            var elCambiado = await _context.Preguntas.FindAsync(cambiado);

            var ordenAntiguoDelTomado = elTomado.Orden;
            elTomado.Orden = elCambiado.Orden;
            elCambiado.Orden = ordenAntiguoDelTomado;

            await _context.SaveChangesAsync();
            return NoContent();
        }

        // PUT: api/Preguntas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPregunta([FromRoute] int id, [FromBody] PreguntaViewModel pregunta)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (pregunta.EsPreguntaSnex)
            {
                var encuestaId = new SqlParameter("@encuestaId", id);
                await _context.Database.ExecuteSqlCommandAsync("update preguntas set EsPreguntaSnex = 0 where encuestaid = @encuestaId", encuestaId);
            }

            var m = await _context.Preguntas.FindAsync(pregunta.Id);
            _mapper.Map(pregunta, m);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // POST: api/Preguntas
        [HttpPost]
        public async Task<IActionResult> PostPregunta(int encuestaId, [FromBody] PreguntaViewModel pregunta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var m = new Pregunta();
            _mapper.Map(pregunta, m);
            m.EncuestaId = encuestaId;

            var preguntasEncuesta = await _context.Preguntas.AsNoTracking().Where(t => t.EncuestaId == encuestaId).ToListAsync();
            m.Orden = preguntasEncuesta.Any() ? (preguntasEncuesta.Min(t => t.Orden) - 1) : 1000;

            _context.Preguntas.Add(m);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetPregunta", new { id = m.Id }, m);
        }

        // DELETE: api/Preguntas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePregunta([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pregunta = await _context.Preguntas.FindAsync(id);
            if (pregunta == null)
            {
                return NotFound();
            }

            _context.Preguntas.Remove(pregunta);
            await _context.SaveChangesAsync();

            return Ok(pregunta);
        }

        private bool PreguntaExists(int id)
        {
            return _context.Preguntas.Any(e => e.Id == id);
        }
    }
}