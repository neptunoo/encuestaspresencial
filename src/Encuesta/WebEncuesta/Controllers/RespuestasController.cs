﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.DynamicLinqCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.ViewModels;
using System.Linq;
using WebEncuesta.Helpers;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RespuestasController : ControllerBase
    {

        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public RespuestasController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpPost]
        public DataSourceResult GetRespuestas(DataSourceRequest request)
        {
            var result = _context.Respuestas
                .Include(t => t.Usuario).Include(t => t.Encuesta)
                .AsNoTracking()
                .Select(p => new RespuestaViewModel
                {
                    Id = p.Id,
                    EncuestaTitulo = p.Encuesta != null ? p.Encuesta.Titulo : string.Empty,
                    RutCliente = p.RutCliente.HasValue ? PresentadoresHelpers.Rut(p.RutCliente.Value) : string.Empty,
                    UsuarioNombre = p.Usuario != null ? p.Usuario.Nombre : string.Empty,
                    Fecha = p.Fecha
                })
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);

            return result;
        }
    }
}