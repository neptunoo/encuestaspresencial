﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreasController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public AreasController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<SelectListItem>> GetAreas()
        {
            return await _context.Areas.AsNoTracking().Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = n.Nombre
            }).ToListAsync();
        }

        [HttpGet("{idArea}/secciones")]
        public async Task<IEnumerable<SelectListItem>> GetSecciones(int idArea)
        {
            return await _context.Secciones.AsNoTracking().Where(t => t.AreaId == idArea).Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = n.Nombre
            }).ToListAsync();
        }
    }
}