﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public UsuariosController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<UsuarioViewModel>> GetUsuarios(int encuestaId)
        {
            var data = await _context.Usuarios.AsNoTracking().Include(t => t.Seccion).OrderBy(t => t.Nombre).ToListAsync();
            var mappedData = _mapper.Map<List<UsuarioViewModel>>(data);
            return mappedData;
        }

        [HttpGet("Secciones")]
        public async Task<IEnumerable<SelectListItem>> GetSecciones()
        {
            return await _context.Secciones.AsNoTracking().Include(t => t.Area).Select(n => new SelectListItem
            {
                Value = n.Id.ToString(),
                Text = $"[{n.Area.Nombre}] {n.Nombre}"
            }).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsuario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuario = await _context.Usuarios.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }


        // PUT: api/Usuarios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario([FromRoute] int id, [FromBody] UsuarioViewModel usuario)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var m = await _context.Usuarios.FindAsync(id);
            _mapper.Map(usuario, m);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // POST: api/Preguntas
        [HttpPost]
        public async Task<IActionResult> PostUsuario([FromBody] UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var m = new Usuario();
            _mapper.Map(usuario, m);
            _context.Usuarios.Add(m);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetUsuario", new { id = m.Id }, m);
        }
    }
}