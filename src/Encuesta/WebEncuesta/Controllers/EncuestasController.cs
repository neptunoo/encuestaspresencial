﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Encuestas;
using WebEncuesta.Helpers.Extensions;
using WebEncuesta.ViewModels;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EncuestasController : ControllerBase
    {
        private readonly WebEncuestaDbContext _context;
        private readonly IMapper _mapper;

        public EncuestasController(WebEncuestaDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        [HttpGet("{usuarioId}")]
        public async Task<EncuestaViewModel> GetEncuesta(int usuarioId)
        {
            var usuario = _context.Usuarios.First(t => t.Id == usuarioId);
            var encuesta = _context.Encuestas
                                .Where(t => t.SeccionId == usuario.SeccionId && DateTime.Now.Date >= t.Vigencia.Desde && DateTime.Now.Date <= t.Vigencia.Hasta)
                                .OrderByDescending(t => t.Vigencia.Hasta)
                                .First();

            var data = await _context.Encuestas.Include(i => i.Preguntas).FirstOrDefaultAsync(t => t.Id == encuesta.Id);
            return _mapper.Map<EncuestaViewModel>(data);
        }

        [HttpPost]
        public async Task<IActionResult> PostEncuesta(RespuestaEncuestaViewModel respuesta)
        {
            var infoEncuesta = await _context.Encuestas.Include(i => i.Preguntas).FirstOrDefaultAsync(t => t.Id == respuesta.EncuestaId);

            Respuesta respuestaGuardable = new Respuesta
            {
                EncuestaId = respuesta.EncuestaId,
                Fecha = DateTime.Now,
                UsuarioId = respuesta.UsuarioId,
                RutCliente = respuesta.Rut,
                Respuestas = new System.Collections.ObjectModel.Collection<RespuestaPregunta>()
            };
            foreach (var item in respuesta.RespuestasPreguntas)
            {
                respuestaGuardable.Respuestas.Add(new RespuestaPregunta
                {
                    PreguntaId = item.PreguntaId,
                    Valor = int.TryParse(item.Valor, out var parser) ? parser : 0,
                    FuePreguntaSnex = infoEncuesta.Preguntas.Single(t => t.Id == item.PreguntaId).EsPreguntaSnex
                });
            }

            await _context.Respuestas.AddAsync(respuestaGuardable);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}