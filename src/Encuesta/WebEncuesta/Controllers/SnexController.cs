﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebEncuesta.Domain.Data;
using WebEncuesta.Domain.Model.Querys;
using WebEncuesta.Domain.Model.Tipos;

namespace WebEncuesta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SnexController : ControllerBase
    {
        private readonly WebEncuestaDbContext _db;

        public SnexController(WebEncuestaDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemSnexPorUsuario>> Get()
        {
            var data = await _db.ItemSnexPorUsuarios.AsNoTracking().FromSql(@"select 
                                                                            are.Nombre AreaNombre,
                                                                            sec.Nombre SeccionNombre,
                                                                            usu.Nombre UsuarioNombre,
                                                                            year(rpt.Fecha) Ano,
                                                                            month(rpt.fecha) Mes,
                                                                            count(trptpr1.Valor) Positivos,
                                                                            count(trptpr3.Valor) Negativos,
                                                                            count(1) Total
                                                                            from Respuestas rpt
                                                                            join Usuarios usu on rpt.UsuarioId = usu.Id
                                                                            join Encuestas enc on rpt.EncuestaId = enc.id
                                                                            join Secciones sec on enc.SeccionId = sec.Id
                                                                            join Areas are on sec.AreaId = are.id
                                                                            left join (select rptpr1.RespuestaId, rptpr1.valor from RespuestasPreguntas rptpr1 where rptpr1.FuePreguntaSnex = 1 and rptpr1.Valor = 1) trptpr1 on trptpr1.RespuestaId = rpt.Id 
                                                                            left join (select rptpr3.RespuestaId, rptpr3.valor from RespuestasPreguntas rptpr3 where rptpr3.FuePreguntaSnex = 1 and rptpr3.Valor = 3) trptpr3 on trptpr3.RespuestaId = rpt.Id 
                                                                            group by are.Nombre,
                                                                            sec.Nombre,
                                                                            usu.Nombre,
                                                                            year(rpt.Fecha),
                                                                            month(rpt.fecha)
                                                                            ").ToListAsync();


            foreach (var each in data)
            {
                each.Snex = (each.Positivos - each.Negativos) / (float)each.Total;
                each.EtiquetaMes = ((TiposMes)each.Mes).ToString();
            }

            return data;
        }
    }
}