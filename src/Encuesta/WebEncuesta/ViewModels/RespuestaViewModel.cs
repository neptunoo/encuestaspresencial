﻿using System;

namespace WebEncuesta.ViewModels
{
    public class RespuestaViewModel
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string UsuarioNombre { get; set; }
        public string RutCliente { get; set; }
        public string EncuestaTitulo { get; set; }
    }
}
