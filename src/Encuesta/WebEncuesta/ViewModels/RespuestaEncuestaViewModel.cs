﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace WebEncuesta.ViewModels
{
    public class RespuestaEncuestaViewModel
    {
        [Required]
        public int Rut { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        [Required]
        public int EncuestaId { get; set; }
        public List<RespuestaPreguntaViewModel> RespuestasPreguntas { get; set; }
    }
}
