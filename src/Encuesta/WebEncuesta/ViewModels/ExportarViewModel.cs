﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebEncuesta.ViewModels
{
    public class ExportarViewModel
    {
        [Display(Name = "Desde")]
        public DateTime? VigenciaDesde{ get; set; }

        [Display(Name = "Hasta")]
        public DateTime? VigenciaHasta { get; set; }

        [Display(Name = "Encuesta")]
        public int EncuestaId { get; set; }

        [Display(Name = "Área")]
        public int? SeccionAreaId { get; set; }

        [Display(Name="Ejecutivo")]
        public int? EjecutivoId { get; set; }

        [Display(Name = "Sección")]
        public int? SeccionId { get; set; }
    }
}
