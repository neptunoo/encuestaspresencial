﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebEncuesta.ViewModels
{
    public class RespuestaPreguntaViewModel
    {
        [Required]
        public int PreguntaId { get; set; }

        [Required]
        public string Valor { get; set; }

    }
}
