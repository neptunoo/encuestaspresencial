﻿namespace WebEncuesta.ViewModels
{
    public class ItemPestanaViewModel
    {
        public ItemPestanaViewModel(string idForQueryParameter, string linkName)
        {
            IdForQueryParameter = idForQueryParameter;
            LinkName = linkName;
        }

        public string IdForQueryParameter { get; set; }
        public string LinkName { get; set; }
    }
}
