﻿using WebEncuesta.Domain.Model.Encuestas;

namespace WebEncuesta.ViewModels
{
    public class PreguntaViewModel
    {
        public int? Id { get; set; }
        public string Glosa { get; set; }
        public TipoPregunta TipoAlternativasPregunta { get; set; }

        public string Valor { get; set; }

        public bool EsPreguntaSnex { get; set; }
        public bool Deshabilitado { get; set; }
        public int Orden { get; set; }

        public int EncuestaId { get; set; }
    }
}
