﻿using WebEncuesta.Helpers.Validations;

namespace WebEncuesta.ViewModels
{
    public class IngresoResponderEncuestaViewModel : IRutViewModel
    {
        [Rut]
        public string Rut { get; set; }

        public string TituloEncuesta { get; set; }

        public string DescripcionEncuesta { get; set; }

        public bool EsZenit { get; set; }
    }
}
