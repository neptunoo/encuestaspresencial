﻿namespace WebEncuesta.ViewModels
{
    public class ItemSnexViewModel
    {
        public string Mes { get; set; }
        public int Respuestas { get; set; }
        public int Encuestas { get; set; }
        public float ValorSnex { get; set; }
    }
}
