﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebEncuesta.ViewModels
{
    public class EncuestaViewModel
    {
        private const string CampoRequerido = "Requerido";

        public int Id { get; set; }
        [Required(ErrorMessage = CampoRequerido)]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = CampoRequerido)]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        public List<PreguntaViewModel> Preguntas { get; set; }

        [Required(ErrorMessage = CampoRequerido)]
        [Display(Name = "Vigente desde")]
        public DateTime? VigenciaDesde { get; set; }

        [Required(ErrorMessage = CampoRequerido)]
        [Display(Name = "Hasta")]
        public DateTime? VigenciaHasta { get; set; }
        public string SeccionNombre { get; set; }

        public string SeccionAreaNombre { get; set; }

        [Display(Name = "Sección")]
        [Required(ErrorMessage = CampoRequerido)]
        public int? SeccionId { get; set; }

        [Display(Name = "Encuesta Zenit")]
        public bool EsCompaniaAlt { get; set; }
    }
}
