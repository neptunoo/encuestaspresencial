﻿using System.ComponentModel.DataAnnotations;
using WebEncuesta.Domain.Model;

namespace WebEncuesta.ViewModels
{
    public class EjecutivoViewModel : IEntidad, IRutViewModel
    {

        public int Id { get; set; }
        [Helpers.Validations.Nombre]
        [Required(ErrorMessage = "Ingresa tu nombre")]
        public string Nombre { get; set; }

        [Helpers.Validations.Rut]
        [Required(ErrorMessage = "Ingresa tu rut")]
        public string Rut { get; set; }

        [Required(ErrorMessage = "Selecciona una sección")]
        [Display(Name = "Sección")]
        public int SeccionId { get; set; }

        public string SeccionNombre { get; set; }

        public Seccion ProxySeccion { get; set; } = new Seccion();
        public bool Deshabilitado { get; set; }
    }
}
