﻿using System.ComponentModel.DataAnnotations;
using WebEncuesta.Domain.Model;


namespace WebEncuesta.ViewModels
{
    public class UsuarioViewModel : IEntidad
    {
     
        public string SeccionAreaNombre { get; set; }

        [Required(ErrorMessage = "Selecciona área")]
        [Display(Name = "Área")]
        public int SeccionAreaId { get; set; }

        public int Id { get; set; }

        [Helpers.Validations.Nombre]
        [Required(ErrorMessage = "Ingresa tu nombre")]
        public string Nombre { get; set; }


        [Required(ErrorMessage = "Selecciona una sección")]
        [Display(Name = "Sección")]
        public int SeccionId { get; set; }


        public string SeccionNombre { get; set; }


        public bool EsAdministrador { get; set; }

        [Required(ErrorMessage = "Ingrese una contraseña")]
        [DataType(DataType.Password)]
        public string Contrasena { get; set; }

        [Required(ErrorMessage = "Vuelva a ingresar la contraseña")]
        [DataType(DataType.Password)]
        [Compare("Contrasena", ErrorMessage = "Las contraseñas no coinciden")]
        public string Confirmacion { get; set; }


        [Required(ErrorMessage = "Ingrese su correo")]
        [EmailAddress(ErrorMessage = "Correo no válido")]
        public string Correo { get; set; }

        public bool Deshabilitado { get; set; }
    }
}
