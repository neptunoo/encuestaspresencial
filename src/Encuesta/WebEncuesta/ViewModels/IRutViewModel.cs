﻿namespace WebEncuesta.ViewModels
{
    public interface IRutViewModel
    {
        string Rut { get; set; }
    }
}
