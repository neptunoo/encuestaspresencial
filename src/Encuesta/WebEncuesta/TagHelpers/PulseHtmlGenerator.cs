﻿using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Options;

namespace WebEncuesta.TagHelpers
{
    public class PulseHtmlGenerator : DefaultHtmlGenerator
    {
        public PulseHtmlGenerator(IAntiforgery antiforgery,
                                    IOptions<MvcViewOptions> optionsAccessor,
                                    IModelMetadataProvider metadataProvider,
                                    IUrlHelperFactory urlHelperFactory,
                                    HtmlEncoder htmlEncoder,
                                    ValidationHtmlAttributeProvider validationAttributeProvider)
            : base(antiforgery, optionsAccessor, metadataProvider, urlHelperFactory, htmlEncoder, validationAttributeProvider)
        {
        }
    }
}
//https://bitbucket.org/neptunoo/auxsalud/commits/333f48408c59b18254fc57c4767da944da84d7cf#chg-src/Salud/WebSalud/Helpers/Tags/InputHasErrorTagHelper.cs