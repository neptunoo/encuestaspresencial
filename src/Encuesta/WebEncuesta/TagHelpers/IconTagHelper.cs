﻿using System.IO;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace WebEncuesta.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("icon", TagStructure = TagStructure.WithoutEndTag)]
    public class IconTagHelper : TagHelper
    {
        public string Name { get; set; }

        public FontAwesomeSet Set { get; set; } = FontAwesomeSet.Solid;
        public string AditionalClass { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "span";

            var classAttribute = output.Attributes["class"];
            var classes = (classAttribute?.Value as HtmlString)?.Value ?? string.Empty;
            classes += " icon";

            if (!string.IsNullOrEmpty(AditionalClass))
                classes += $" {AditionalClass}";

            classes = classes.Trim();
            output.Attributes.SetAttribute("class", classes);

            output.TagMode = TagMode.StartTagAndEndTag;

            var svg = File.ReadAllText($"Assets/FontAwesome/{this.Set.ToString().ToLowerInvariant()}/{this.Name}.svg");
            output.Content.SetHtmlContent(svg);
        }
    }

    public enum FontAwesomeSet
    {
        Solid,
        Regular,
        Brands
    }
}
