﻿using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace WebEncuesta.TagHelpers
{
    [HtmlTargetElement("a", Attributes = "pulse-activar-si")]
    public class PestanaTagHelper : TagHelper
    {
        [HtmlAttributeName("pulse-activar-si")]
        public bool Active { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (Active)
            {
                var existingClass = output.Attributes.FirstOrDefault(f => f.Name == "class");
                var cssClass = string.Empty;
                if (existingClass != null)
                {
                    cssClass = existingClass.Value.ToString();
                }
                cssClass = cssClass + " active";
                var ta = new TagHelperAttribute("class", cssClass);
                output.Attributes.Remove(existingClass);
                output.Attributes.Add(ta);
            }
        }
    }
}
